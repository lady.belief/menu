import React from 'react'
import { Typography } from '@mui/material';

function AboutPage() {
    return (
        <homepage>
            <div>
                <Typography>Cиcтема просмотра меню ресторана.</Typography>
                <Typography>Этот проект разработан в рамках обучения на курсе ASP.Net Core в OTUS.</Typography>
                <Typography>Автор: Вера Большакова</Typography>
            </div>
        </homepage>
    )
}

export default AboutPage