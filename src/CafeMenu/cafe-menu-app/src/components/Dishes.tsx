import React, { useEffect, useState } from 'react';
import { Card } from '@mui/material';
import { HttpTransportType, HubConnection, HubConnectionBuilder, LogLevel } from "@microsoft/signalr";

type Dish = {
    name: string,
    description: string, //Описание для сайта
    recipe: string, //Описание процесса приготовления
    isAvailable: boolean, //Доступно ли
    foodstuff: any
}

const Dishes = () => {
    const [dishes, setDishes] = useState<Dish[]>(() => []);
    const [connection, setConnection] = useState<null | HubConnection>(null);

    const updateDishes = (allNewDishes: Dish[]) => {
        setDishes(dishes => []);
        for(let i=0; i<allNewDishes.length; i++) {
            if(allNewDishes[i].isAvailable)
            {
                setDishes(dishes => dishes.concat(allNewDishes[i]));
            }
        }
    }

    const getDishes = () => {
        try {
            fetch(`${process.env.REACT_APP_BACKEND_URL || ''}/api/v1/Dish`).then(async response => {
                const allDishes: Dish[] = await response.json();
                updateDishes(allDishes);
            }).catch(e=>{
                console.log(JSON.stringify(e)); 
            });
        } catch (e) {
            console.log(JSON.stringify(e)); 
        }
    }

    useEffect(() => {
        getDishes();
    }, []);

    useEffect(() => {
        console.log("start");
    
        const connect = new HubConnectionBuilder()
          .configureLogging(LogLevel.Debug)
          .withUrl(`${process.env.REACT_APP_BACKEND_URL || ''}/hubs/dishes`, {
            skipNegotiation: true,
            transport: HttpTransportType.WebSockets
          })
          .withAutomaticReconnect()
          .build();
    
        setConnection(connect);
      }, []);
    
      useEffect(() => {
        if (connection) {
          connection
            .start()
            .then(() => {
              console.log("connect to stock info");
              connection.on("ReceiveDishes", handleReceiveMessage);
            })
            .catch((error) => console.log(error));
        }
      }, [connection]);
    
      const handleReceiveMessage = (newDishes: Dish[]) => {
        updateDishes(newDishes);
      }

    return (
        <div>
            {dishes.map(dish => 
                <Card elevation={2}>
                    <dt><h4>{dish.name}</h4></dt>
                    <dd>{dish.description}</dd>
                </Card>
            )}
        </div>
    );
}

export default Dishes;