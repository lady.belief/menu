import React, { useState } from 'react';
import './App.css';
import { BrowserRouter, Routes, Route, Link } from 'react-router-dom';
import Dishes from './components/Dishes';
import AboutPage from './components/About';

function App() {
  return (
    <BrowserRouter>
      <nav>
          <li>
            <Link to={'/dishes'}>Меню</Link>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <Link to={'/about'}>О проекте</Link>
          </li>
      </nav>
      
      <Routes>
        <Route path="dishes" element={<Dishes />} />
        <Route path="about" element={<AboutPage />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
