﻿using CafeMenu.Core.Domain;
using Microsoft.EntityFrameworkCore;

namespace CafeMenu.DataAccess;

public class DataContext : DbContext
{
    public DbSet<Foodstuff> Foodstuffs { get; set; }
    public DbSet<Dish> Dishes { get; set; }
    
    public DataContext()
    { }
        
    public DataContext(DbContextOptions<DataContext> options)
        : base(options)
    { }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    { }
}