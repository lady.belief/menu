﻿using CafeMenu.Core.Domain;

namespace CafeMenu.DataAccess.Data
{
    public static class FakeDataFactory
    {
        public static List<Foodstuff> Foodstuffs => new List<Foodstuff>()
        {
            new Foodstuff()
            {
                Id = Guid.Parse("b52d4a0f-3ecb-4305-9e23-7a7b32773ac4"),
                Name = "ветчина",
                IsAvailable = true
            },
            new Foodstuff()
            {
                Id = Guid.Parse("2669d6dd-5efa-4f1a-b8ce-ad686c2a5b0f"),
                Name = "моцарелла",
                IsAvailable = true
            },
            new Foodstuff()
            {
                Id = Guid.Parse("6421a38f-2679-478d-ad5a-d2c60d31a7a7"),
                Name = "фирменный соус",
                IsAvailable = true
            },
            new Foodstuff()
            {
                Id = Guid.Parse("332ddf3f-ba9f-4407-9a28-37d58f69d921"),
                Name = "цыпленок",
                IsAvailable = true
            },
            new Foodstuff()
            {
                Id = Guid.Parse("89324170-16d4-444e-8822-107d084fd12d"),
                Name = "бекон",
                IsAvailable = false
            },
            new Foodstuff()
            {
                Id = Guid.Parse("453cbff0-88c3-47d9-af80-a18eb7d0c74a"),
                Name = "сыр чеддер",
                IsAvailable = true
            },
            new Foodstuff()
            {
                Id = Guid.Parse("46a84513-6654-4ec8-bf78-41f8642d194b"),
                Name = "сыр пармезан",
                IsAvailable = true
            },
            new Foodstuff()
            {
                Id = Guid.Parse("1d322cc0-8d8a-41aa-b938-90b8601605d7"),
                Name = "томаты",
                IsAvailable = true
            },
            new Foodstuff()
            {
                Id = Guid.Parse("e663c5e4-55b2-4e5c-b977-ce0cd9c89373"),
                Name = "красный лук",
                IsAvailable = true
            },
            new Foodstuff()
            {
                Id = Guid.Parse("91744af7-3e69-4f23-bd76-7b1153e607b7"),
                Name = "чеснок",
                IsAvailable = true
            },
            new Foodstuff()
            {
                Id = Guid.Parse("31915fff-b707-4f82-9b2e-94522fe86e11"),
                Name = "итальянские травы",
                IsAvailable = true
            }
        };

        public static List<Dish> Dishes => new List<Dish>()
        {
            new Dish("Карбонара")
            {
                Id = Guid.Parse("af314498-3d61-4f7d-bf67-42d2e8657082"),
                Description =
                    "Бекон, сыры чеддер и пармезан, моцарелла, томаты, красный лук, чеснок, фирменный соус, итальянские травы",
                Recipe = "Просто брось всё на тесто",
                IsAvailable = false,
                FoodstuffList = FoodstuffsDish1
            },
            new Dish("Ветчина и сыр")
            {
                Id = Guid.Parse("337584de-9ed6-42da-be70-bd6e032c6110"),
                Description = "Ветчина, моцарелла, фирменный соус",
                Recipe = "Просто брось всё на тесто",
                IsAvailable = true,
                FoodstuffList = FoodstuffsDish2
            },
            new Dish("Двойной цыпленок")
            {
                Id = Guid.Parse("c5727a11-a058-4817-832a-5f0791152c7d"),
                Description = "Цыпленок, моцарелла, фирменный соус",
                Recipe = "Просто брось всё на тесто",
                IsAvailable = true,
                FoodstuffList = FoodstuffsDish3
            }
        };

        public static List<FoodstuffDish> FoodstuffsDish1 => new List<FoodstuffDish>()
        {
            new FoodstuffDish()
            {
                Id = Guid.NewGuid(),
                DishId = Guid.Parse("af314498-3d61-4f7d-bf67-42d2e8657082"),
                FoodstuffId = Guid.Parse("89324170-16d4-444e-8822-107d084fd12d")
            },
            new()
            {
                Id = Guid.NewGuid(), 
                DishId = Guid.Parse("af314498-3d61-4f7d-bf67-42d2e8657082"), 
                FoodstuffId = Guid.Parse("453cbff0-88c3-47d9-af80-a18eb7d0c74a")
            },
            new()
            {
                Id = Guid.NewGuid(), 
                DishId = Guid.Parse("af314498-3d61-4f7d-bf67-42d2e8657082"), 
                FoodstuffId = Guid.Parse("46a84513-6654-4ec8-bf78-41f8642d194b")
            },
            new()
            {
                Id = Guid.NewGuid(), 
                DishId = Guid.Parse("af314498-3d61-4f7d-bf67-42d2e8657082"), 
                FoodstuffId = Guid.Parse("2669d6dd-5efa-4f1a-b8ce-ad686c2a5b0f")
            },
            new()
            {
                Id = Guid.NewGuid(), 
                DishId = Guid.Parse("af314498-3d61-4f7d-bf67-42d2e8657082"), 
                FoodstuffId = Guid.Parse("1d322cc0-8d8a-41aa-b938-90b8601605d7")
            },
            new()
            {
                Id = Guid.NewGuid(), 
                DishId = Guid.Parse("af314498-3d61-4f7d-bf67-42d2e8657082"), 
                FoodstuffId = Guid.Parse("e663c5e4-55b2-4e5c-b977-ce0cd9c89373")
            },
            new()
            {
                Id = Guid.NewGuid(), 
                DishId = Guid.Parse("af314498-3d61-4f7d-bf67-42d2e8657082"), 
                FoodstuffId = Guid.Parse("91744af7-3e69-4f23-bd76-7b1153e607b7")
            },
            new()
            {
                Id = Guid.NewGuid(), 
                DishId = Guid.Parse("af314498-3d61-4f7d-bf67-42d2e8657082"), 
                FoodstuffId = Guid.Parse("31915fff-b707-4f82-9b2e-94522fe86e11")
            },
            new()
            {
                Id = Guid.NewGuid(), 
                DishId = Guid.Parse("af314498-3d61-4f7d-bf67-42d2e8657082"), 
                FoodstuffId = Guid.Parse("6421a38f-2679-478d-ad5a-d2c60d31a7a7")
            }
        };

        public static List<FoodstuffDish> FoodstuffsDish2 => new List<FoodstuffDish>()
        {
            new FoodstuffDish()
            {
                Id = Guid.NewGuid(),
                DishId = Guid.Parse("337584de-9ed6-42da-be70-bd6e032c6110"),
                FoodstuffId = Guid.Parse("b52d4a0f-3ecb-4305-9e23-7a7b32773ac4")
            },
            new()
            {
                Id = Guid.NewGuid(), 
                DishId = Guid.Parse("337584de-9ed6-42da-be70-bd6e032c6110"), 
                FoodstuffId = Guid.Parse("2669d6dd-5efa-4f1a-b8ce-ad686c2a5b0f")
            },
            new()
            {
                Id = Guid.NewGuid(), 
                DishId = Guid.Parse("337584de-9ed6-42da-be70-bd6e032c6110"), 
                FoodstuffId = Guid.Parse("6421a38f-2679-478d-ad5a-d2c60d31a7a7")
            }
        };

        public static List<FoodstuffDish> FoodstuffsDish3 => new List<FoodstuffDish>()
        {
            new FoodstuffDish()
            {
                Id = Guid.NewGuid(),
                DishId = Guid.Parse("c5727a11-a058-4817-832a-5f0791152c7d"),
                FoodstuffId = Guid.Parse("332ddf3f-ba9f-4407-9a28-37d58f69d921")
            },
            new()
            {
                Id = Guid.NewGuid(), 
                DishId = Guid.Parse("c5727a11-a058-4817-832a-5f0791152c7d"), 
                FoodstuffId = Guid.Parse("2669d6dd-5efa-4f1a-b8ce-ad686c2a5b0f")
            },
            new()
            {
                Id = Guid.NewGuid(), 
                DishId = Guid.Parse("c5727a11-a058-4817-832a-5f0791152c7d"), 
                FoodstuffId = Guid.Parse("6421a38f-2679-478d-ad5a-d2c60d31a7a7")
            }
        };
    }
}