﻿namespace CafeMenu.DataAccess.Data
{
    public interface IDbInitializer
    {
        void InitializeDb();
    }
}