﻿namespace CafeMenu.DataAccess.Data
{
    public class EfDbInitializer
        : IDbInitializer
    {
        private readonly DataContext _dataContext;

        public EfDbInitializer(DataContext dataContext)
        {
            _dataContext = dataContext;
        }
        
        public void InitializeDb()
        {
            _dataContext.Database.EnsureDeleted();
            _dataContext.Database.EnsureCreated();

            _dataContext.AddRange(FakeDataFactory.Foodstuffs);
            _dataContext.SaveChanges();
            
            _dataContext.AddRange(FakeDataFactory.Dishes);
            _dataContext.SaveChanges();
            
            // _dataContext.AddRange(FakeDataFactory.FoodstuffsDish);
            // _dataContext.SaveChanges();
        }
    }
}