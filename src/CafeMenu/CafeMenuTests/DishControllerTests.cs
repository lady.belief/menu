using CafeMenu.Core.Domain;
using CafeMenu.Core.Services;
using CafeMenu.DataAccess.Data;
using CafeMenu.WebHost.Controllers;
using Microsoft.AspNetCore.Mvc;

namespace CafeMenuTests;

public class DishControllerTests
{
    private readonly DishController _controllerDish;
    private readonly FoodstuffService _serviceFoodstuff;
    private readonly DishService _serviceDish;
    
    public DishControllerTests()
    {
        var dishRepositoryMock = new MockRepositoryBuilder<Dish>()
            .EnableGetByIdAsync()
            .EnableUpdateAsync()
            .SetData(FakeDataFactory.Dishes)
            .Build();
        var foodstuffDishRepositoryMock = new MockRepositoryBuilder<FoodstuffDish>()
            .EnableGetByIdAsync()
            .EnableUpdateAsync()
            .SetData(FakeDataFactory.FoodstuffsDish2)
            .Build();
        
        var foodstuffRepositoryMock = new MockRepositoryBuilder<Foodstuff>()
            .EnableGetByIdAsync()
            .EnableUpdateAsync()
            .SetData(FakeDataFactory.Foodstuffs)
            .Build();

        _serviceDish = new DishService(null, null, foodstuffDishRepositoryMock.Object);
        _controllerDish = new DishController(dishRepositoryMock.Object, 
            foodstuffDishRepositoryMock.Object, _serviceDish, null, null);
        _serviceFoodstuff = new FoodstuffService(foodstuffRepositoryMock.Object, dishRepositoryMock.Object,
            foodstuffDishRepositoryMock.Object, null, null);
    }
    
    [Fact]
    public async void DishIsDisabledAfterFoodstuffDisabling()
    {
        var dishId = Guid.Parse("337584de-9ed6-42da-be70-bd6e032c6110");
        
        var res = await _controllerDish.GetDishAsync(dishId);
        var dish = (res.Result as OkObjectResult)?.Value as Dish;
        Assert.True(dish is { IsAvailable: true });

        var foodstuffData = FakeDataFactory.Foodstuffs[0];
        Assert.True(foodstuffData.IsAvailable);
        var request = new FoodstuffDtoAlias(foodstuffData.Id, false);
        await _serviceFoodstuff.SetFoodstuffAndDishAvailabilityAsync(request);
        
        res = await _controllerDish.GetDishAsync(dishId);
        dish = (res.Result as OkObjectResult)?.Value as Dish;
        Assert.True(dish is { IsAvailable: false });
        
    }

    [Fact]
    public async void DisableAllFoodstuffs_EnableAllFoodstuffs_DishEnabling()
    {
        ActionResult<Dish> res;
        Dish? dish;
        var dishId = Guid.Parse("337584de-9ed6-42da-be70-bd6e032c6110");

        foreach (var foodstuffData in FakeDataFactory.Foodstuffs)
        {
            if (foodstuffData.IsAvailable)
            {
                var request = new FoodstuffDtoAlias(foodstuffData.Id, false);
                await _serviceFoodstuff.SetFoodstuffAndDishAvailabilityAsync(request);
            }
        }

        foreach (var foodstuffData in FakeDataFactory.Foodstuffs)
        {
            res = await _controllerDish.GetDishAsync(dishId);
            dish = (res.Result as OkObjectResult)?.Value as Dish;
            Assert.True(dish is { IsAvailable: false });
            
            var request = new FoodstuffDtoAlias(foodstuffData.Id, true);
            await _serviceFoodstuff.SetFoodstuffAndDishAvailabilityAsync(request);
        }

        res = await _controllerDish.GetDishAsync(dishId);
        dish = (res.Result as OkObjectResult)?.Value as Dish;
        Assert.True(dish is { IsAvailable: true });
    }

    [Fact]
    public async void DeleteDish_DishFoodstuffDeleting()
    {
        var dishId = Guid.Parse("337584de-9ed6-42da-be70-bd6e032c6110");
        var dish = await _controllerDish.GetDishAsync(dishId);
        Assert.NotNull(dish);
        Assert.NotEmpty(await _serviceDish.GetIngredientsAsync(dishId));

        await _controllerDish.DeleteDishAsync(dishId);
        
        var res = await _controllerDish.GetDishesAsync();
        var dishList = (res.Result as OkObjectResult)?.Value as List<Dish>;
        if (dishList != null) Assert.DoesNotContain(dishList, d => d.Id == dishId);
        Assert.Empty(await _serviceDish.GetIngredientsAsync(dishId));
    }
}