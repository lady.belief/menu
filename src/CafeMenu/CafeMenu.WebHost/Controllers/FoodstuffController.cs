﻿using CafeMenu.Core.Abstractions;
using CafeMenu.Core.Domain;
using CafeMenu.Core.Services;
using CafeMenu.WebHost.Models;
using Microsoft.AspNetCore.Mvc;

namespace CafeMenu.WebHost.Controllers;

[ApiController]
[Route("api/v1/[controller]")]
public class FoodstuffController : ControllerBase
{
    private readonly IRepository<Foodstuff> _foodstuffRepository;
    private readonly IFoodstuffService _foodstuffService;

    public FoodstuffController(IRepository<Foodstuff> foodstuffRepository, IFoodstuffService foodstuffService)
    {
        _foodstuffRepository = foodstuffRepository;
        _foodstuffService = foodstuffService;
    }
    
    /// <summary>
    /// Получить список продуктов
    /// </summary>
    /// <returns></returns>
    [HttpGet]
    public async Task<ActionResult<List<Foodstuff>>> GetFoodstuffsAsync()
    {
        var foodstuffs =  await _foodstuffRepository.GetAllAsync();

        return Ok(foodstuffs);
    }
    
    /// <summary>
    /// Получить продукт по id
    /// </summary>
    /// <param name="id">Id продукта, например <example>b52d4a0f-3ecb-4305-9e23-7a7b32773ac4</example></param>
    /// <returns></returns>
    [HttpGet("{id:guid}")]
    public async Task<ActionResult<Foodstuff>> GetFoodstuffAsync(Guid id)
    {
        var foodstuff =  await _foodstuffRepository.GetByIdAsync(id);
        if (foodstuff == null) return NotFound();
        
        return Ok(foodstuff);
    }
        
    /// <summary>
    /// Создать новый продукт
    /// </summary>
    /// <returns></returns>
    [HttpPost]
    public async Task CreateFoodstuffAsync(FoodstuffRequestCreateUpdate request)
    {
        await _foodstuffRepository.AddAsync(new Foodstuff()
        {
            Name = request.Name, 
            IsAvailable = request.IsAvailable
        });
    }
        
    /// <summary>
    /// Обновить продукт
    /// </summary>
    /// <param name="id">Id продукта, например <example>b52d4a0f-3ecb-4305-9e23-7a7b32773ac4</example></param>
    /// <param name="request">Данные запроса></param>
    [HttpPut("{id:guid}")]
    public async Task<IActionResult> EditFoodstuffAsync(Guid id, FoodstuffRequestCreateUpdate request)
    {
        var foodstuff = await _foodstuffRepository.GetByIdAsync(id);
        if (foodstuff == null) return NotFound();

        if (foodstuff.Name == request.Name && foodstuff.IsAvailable == request.IsAvailable)
            return NoContent();
        
        foodstuff.Name = request.Name;
        if (foodstuff.IsAvailable != request.IsAvailable)
        {
            foodstuff.IsAvailable = request.IsAvailable;
            //обновить блюдо, если изменилась доступность продукта
            await _foodstuffService.SetDishAvailability(foodstuff.Id, foodstuff.IsAvailable);
        }
        await _foodstuffRepository.UpdateAsync(foodstuff);

        return Ok();
    }
        
    /// <summary>
    /// Удалить продукт
    /// </summary>
    /// <param name="id">Id продукта, например <example>b52d4a0f-3ecb-4305-9e23-7a7b32773ac4</example></param>
    [HttpDelete("{id:guid}")]
    public async Task<IActionResult> DeleteFoodstuffAsync(Guid id)
    {
        var foodstuff = await _foodstuffRepository.GetByIdAsync(id);
        if (foodstuff == null) return NotFound();

        //сначала обновить блюдо
        await _foodstuffService.SetDishAvailability(id, false);
        //потом удалить продукт
        await _foodstuffRepository.DeleteAsync(foodstuff);

        return Ok();
    }
}