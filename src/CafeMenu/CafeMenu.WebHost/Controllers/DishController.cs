﻿using System.Text.Json;
using CafeMenu.Core.Abstractions;
using CafeMenu.Core.Domain;
using CafeMenu.Core.Services;
using CafeMenu.Core.SignalRHub;
using CafeMenu.WebHost.Mappers;
using CafeMenu.WebHost.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Caching.Distributed;

namespace CafeMenu.WebHost.Controllers;

[ApiController]
[Route("api/v1/[controller]")]
public class DishController : ControllerBase
{
    private readonly IRepository<Dish> _dishRepository;
    private readonly IRepository<FoodstuffDish> _foodstuffDishRepository;
    private readonly IDishService _dishService;

    private readonly ICacheService<Dish>? _cacheService;

    private readonly IHubContext<CafeMenuHub, IDishesHubClient> _hubContextSignalR;

    public DishController(IRepository<Dish> dishRepository,
        IRepository<FoodstuffDish> foodstuffDishRepository, IDishService dishService, 
        ICacheService<Dish> cacheService, IHubContext<CafeMenuHub, IDishesHubClient> hubContextSignalR)
    {
        _dishRepository = dishRepository;
        _foodstuffDishRepository = foodstuffDishRepository;
        _dishService = dishService;
        _cacheService = cacheService;
        _hubContextSignalR = hubContextSignalR;
    }
    
    /// <summary>
    /// Получить список блюд
    /// </summary>
    /// <returns></returns>
    [HttpGet]
    public async Task<ActionResult<List<Dish>>> GetDishesAsync()
    {
        if (_cacheService != null)
        {
            var cacheResult = await _cacheService.GetFromCache();
            if (cacheResult != null)
            {
                return cacheResult;
            }
        }

        var dishes =  (await _dishRepository.GetAllAsync()).ToList();
        if (_cacheService != null)
        {
            await _cacheService.SetToCache(dishes);
        }

        return Ok(dishes);
    }
    
    /// <summary>
    /// Получить блюдо по id
    /// </summary>
    /// <param name="id">Id блюда, например <example>337584de-9ed6-42da-be70-bd6e032c6110</example></param>
    /// <returns></returns>
    [HttpGet("{id:guid}")]
    public async Task<ActionResult<Dish>> GetDishAsync(Guid id)
    {
        var dish =  await _dishRepository.GetByIdAsync(id);
        if (dish == null) return NotFound();
        
        dish.FoodstuffList = await _dishService.GetIngredientsAsync(id);
        
        return Ok(dish);
    }
        
    /// <summary>
    /// Создать новое блюдо
    /// </summary>
    /// <returns></returns>
    [HttpPost]
    public async Task<ActionResult> CreateDishAsync(DishRequestCreateUpdate request)
    {
        var dish = MapperDish.DishFromRequest(request);
        await _dishRepository.AddAsync(dish);

        RemoveCache();
        SendToHub();
        
        return Ok();
    }
        
    /// <summary>
    /// Обновить блюдо
    /// </summary>
    /// <param name="id">Id продукта, например <example>337584de-9ed6-42da-be70-bd6e032c6110</example></param>
    /// <param name="request">Данные запроса></param>
    [HttpPut("{id:guid}")]
    public async Task<IActionResult> EditDishAsync(Guid id, DishRequestCreateUpdate request)
    {
        var dish = await _dishRepository.GetByIdAsync(id);
        if (dish == null) return NotFound();

        await _dishRepository.UpdateAsync(MapperDish.CopyDishFromRequest(dish, request));

        RemoveCache();
        SendToHub();

        return Ok(id);
    }
        
    /// <summary>
    /// Удалить блюдо
    /// </summary>
    /// <param name="id">Id блюда, например <example>337584de-9ed6-42da-be70-bd6e032c6110</example></param>
    [HttpDelete("{id:guid}")]
    public async Task<IActionResult> DeleteDishAsync(Guid id)
    {
        var dish = await _dishRepository.GetByIdAsync(id);
        if (dish == null) return NotFound();

        var foodstuffDishList = await _dishService.GetIngredientsAsync(id);
        foreach (var fd in foodstuffDishList)
        {
            await _foodstuffDishRepository.DeleteAsync(fd);
        }
        
        await _dishRepository.DeleteAsync(dish);
        
        RemoveCache();
        SendToHub();

        return Ok(id);
    }
    
    /// <summary>
    /// Приготовить блюдо с идентификатором id
    /// </summary>
    /// <param name="id">Id блюда, например <example>337584de-9ed6-42da-be70-bd6e032c6110</example></param>
    /// <returns></returns>
    [HttpPut("cook/{id:guid}")]
    public async Task<IActionResult> CookDishAsync(Guid id)
    {
        var dish = await _dishRepository.GetByIdAsync(id);
        if (dish == null) return NotFound();
        if (!dish.IsAvailable) return BadRequest("Dish is not available.");

        var foodstuffDishList = await _dishService.GetIngredientsAsync(id);
        
        //todo amount надо брать из таблицы
        const double amount = 0.05;
        foreach (var foodstuff in foodstuffDishList)
        {
            await _dishService.UpdateSlices(foodstuff.FoodstuffId, amount);
        }
        
        return Ok(id);
    }

    private async void RemoveCache()
    {
        if (_cacheService == null)
            return;

        await _cacheService.RemoveCache();
    }
    
    private async void SendToHub()
    {
        if(_hubContextSignalR == null)
            return;
        
        var allDishes = (await _dishRepository.GetAllAsync()).ToList();
        await _hubContextSignalR.Clients.All.ReceiveDishes(allDishes);
    }
}