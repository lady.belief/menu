﻿namespace CafeMenu.WebHost.Models;

public class DishRequestCreateUpdate
{
    public string Name { get; set; }
    public string? Description { get; set; } // Описание для сайта
    public string? Recipe { get; set; } //Описание процесса приготовления
    public bool IsAvailable  { get; set; } //Доступно ли
    
    public List<Guid> Foodstuffs { get; set; } //Список продуктов
    //public List<double> FoodstuffAmount { get; set; }
    
    public DishRequestCreateUpdate(string name, List<Guid> foodstuffs, /*List<double> amount, */
        string description, string recipe, bool isAvailable)
    {
        Name = name;
        Foodstuffs = foodstuffs;
        Description = description;
        Recipe = recipe;
        IsAvailable = isAvailable;
        //FoodstuffAmount = amount;
    }
}