﻿namespace CafeMenu.WebHost.Models;

public class FoodstuffRequestCreateUpdate
{
    public string Name { get; set; }
    public bool IsAvailable { get; set; }
    
    public FoodstuffRequestCreateUpdate()
    {
        Name = string.Empty;
        IsAvailable = false;
    }
}