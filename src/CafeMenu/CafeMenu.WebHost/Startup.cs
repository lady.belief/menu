using System.Reflection;
using CafeMenu.Core.Abstractions;
using CafeMenu.Core.Consumers;
using CafeMenu.Core.Domain;
using CafeMenu.Core.Loggers;
using CafeMenu.Core.Services;
using CafeMenu.Core.SignalRHub;
using CafeMenu.DataAccess;
using CafeMenu.DataAccess.Data;
using CafeMenu.DataAccess.Repositories;
using CafeMenu.WebHost.Controllers;
using CafeMenu.WebHost.Middlewares;
using CafeMenu.WebHost.Validators;
using FluentValidation;
using FluentValidation.AspNetCore;
using MassTransit;
using Microsoft.AspNetCore.Http.Connections;
using Microsoft.AspNetCore.SpaServices.ReactDevelopmentServer;
using Microsoft.EntityFrameworkCore;
using MongoDB.Driver;
using MongoDbCache;

namespace CafeMenu.WebHost
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton(typeof(ILogger<DishService>), typeof(LoggerDish<DishService>));
            services.AddSingleton(typeof(ILogger<DishConsumer>), typeof(LoggerDish<DishConsumer>));

            services.AddControllers().AddMvcOptions(x =>
                x.SuppressAsyncSuffixInActionNames = false);

            services.AddScoped(typeof(IRepository<>), typeof(EfRepository<>));
            services.AddScoped<IDbInitializer, EfDbInitializer>();

            services.AddSingleton<Foodstuff>();

            services.AddDbContext<DataContext>(x => { x.UseSqlite("Filename=CafeMenu.sqlite"); });

            #region Message Broker
            services.AddTransient<IDishService, DishService>(); //provider
            services.AddTransient<IFoodstuffService, FoodstuffService>(); //consumer
            services.AddMassTransit(x =>
            {
                x.AddConsumer<DishConsumer>();
                x.UsingRabbitMq((context, cfg) =>
                {
                    Configure(cfg);
                    RegisterEndPoints(context, cfg);
                });
            });
            services.AddHostedService<MasstransitService>();
            #endregion

            #region Cache with MongoDb
            services.AddSingleton(typeof(ICacheService<Dish>), typeof(CacheService<Dish>));
            services.AddDistributedMemoryCache();
            var configuration = GetConfiguration();
            var configurationCache = configuration.GetSection("DishesCacheMongoDb");
            services.AddMongoDbCache(options =>
            {
                options.ConnectionString = !IsContainer()
                    ? configurationCache.GetSection("ConnectionString").Value
                    : configurationCache.GetSection("ConnectionStringInContainer").Value;
                options.DatabaseName = configurationCache.GetSection("DatabaseName").Value;
                options.CollectionName = configurationCache.GetSection("CollectionName").Value;
            });
            #endregion

            #region Validation
            // services.AddValidatorsFromAssemblyContaining<ValidatorFoodstuffRequestCreateUpdate>();
            // services.AddValidatorsFromAssemblyContaining<ValidatorDishRequestCreateUpdate>();
            services.AddFluentValidation(fv =>
            {
                fv.RegisterValidatorsFromAssemblyContaining<ValidatorFoodstuffRequestCreateUpdate>();
                fv.RegisterValidatorsFromAssemblyContaining<ValidatorDishRequestCreateUpdate>();
            });
            #endregion

            #region React
            services.AddCors(options =>
            {
                var configurationReact = configuration.GetSection("CORS");
                options.AddPolicy(name: Origin,
                    builder => {
                        builder.WithOrigins(configurationReact.GetSection("Origins").Get<string[]>())
                            .SetIsOriginAllowed((host) => true)
                            .WithHeaders(configurationReact.GetSection("Headers").Get<string[]>())
                            .WithMethods(configurationReact.GetSection("Methods").Get<string[]>());
                    });
            });
            services.AddControllersWithViews();
            services.AddSpaStaticFiles(config =>
            {
                config.RootPath = "../cafe-menu-app/build";
            });
            #endregion

            #region SignalR
            services.AddSignalR(options =>
            {
                options.EnableDetailedErrors = true;
                options.KeepAliveInterval = TimeSpan.FromSeconds(30); //сколько времени ждать чтобы отправить пинг, если не было сообщений
            });
            #endregion
            // Register the Swagger services
            services.AddSwaggerDocument(conf => 
                conf.Title = Assembly.GetExecutingAssembly().GetName().Name);
        }
        
        /// <summary>
        /// Конфигурирование
        /// </summary>
        /// <param name="configurator"></param>
        private static void Configure(IRabbitMqBusFactoryConfigurator configurator)
        {
            var hostName = !IsContainer() ? "localhost" : "rabbitmq";
            configurator.Host(hostName, "/",
                h =>
                {
                    h.Username("rabbitForMenu");
                    h.Password("rabbitForMenu");
                    h.Heartbeat(TimeSpan.FromMinutes(1));
                });
        }

        /// <summary>
        /// регистрация эндпоинтов
        /// </summary>
        /// <param name="context"></param>
        /// <param name="configurator"></param>
        private static void RegisterEndPoints(IBusRegistrationContext context, IRabbitMqBusFactoryConfigurator configurator)
        {
            configurator.ReceiveEndpoint($"UpdateMenu", e =>
            {
                e.ConfigureConsumer<DishConsumer>(context);
                e.UseMessageRetry(r =>
                {
                    r.Incremental(3, TimeSpan.FromSeconds(1), TimeSpan.FromSeconds(1));
                });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IDbInitializer dbInitializer)
        {
            app.UseHttpRequestLogging();
            
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseOpenApi();
            app.UseSwaggerUi3(x =>
            {
                x.DocExpansion = "list";
            });

            //app.UseHttpsRedirection();
            app.UseDefaultFiles();
            app.UseStaticFiles();
            //app.UseSpaStaticFiles();

            app.UseRouting();
            app.UseCors(Origin);

            app.UseEndpoints(endpoints =>
            {
                //endpoints.MapGet("/", async context => { await context.Response.WriteAsync("Hello World!"); });
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller}/{action=Index}/{id?}");
                endpoints.MapHub<CafeMenuHub>("/hubs/dishes", options =>
                {
                    options.ApplicationMaxBufferSize = 1 * 1024 * 1024;

                    options.Transports = HttpTransportType.LongPolling |
                                         HttpTransportType.WebSockets |
                                         HttpTransportType.ServerSentEvents;

                    options.LongPolling.PollTimeout = TimeSpan.FromSeconds(30);
                });
                endpoints.MapControllers();
            });

            /*app.UseSpa(spa =>
            {
                spa.Options.SourcePath = "../cafe-menu-app";

                if (env.IsDevelopment())
                {
                    spa.UseReactDevelopmentServer(npmScript: "start:normal");
                }
            });*/
            
            dbInitializer.InitializeDb();
        }

        private IConfiguration GetConfiguration()
        {
            return new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddEnvironmentVariables()
                .Build();
        }

        private static bool IsContainer()
        {
            return bool.TryParse(Environment.GetEnvironmentVariable("DOTNET_RUNNING_IN_CONTAINER"), out var inDocker) && inDocker;
        }
        
        private readonly string Origin = "MyAllowSpecificOrigins";
    }
}