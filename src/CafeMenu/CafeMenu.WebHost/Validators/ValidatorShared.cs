﻿using System.Text.RegularExpressions;

namespace CafeMenu.WebHost.Validators;

public static class ValidatorShared
{
    public static bool IsName(string s)
    {
        const string pattern = @"[а-яёА-ЯЁ]+([ \.-]{0,2}[а-яёА-ЯЁ])+[\.]{0,2}";
        return Regex.IsMatch(s, pattern, RegexOptions.IgnoreCase);
    }
}