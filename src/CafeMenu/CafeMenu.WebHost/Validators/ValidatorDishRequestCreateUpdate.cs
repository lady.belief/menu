﻿using System.Text.RegularExpressions;
using CafeMenu.WebHost.Models;
using FluentValidation;

namespace CafeMenu.WebHost.Validators;

public class ValidatorDishRequestCreateUpdate : AbstractValidator<DishRequestCreateUpdate>
{
    public ValidatorDishRequestCreateUpdate()
    {
        RuleFor(x => x.Name).NotEmpty();
        RuleFor(x => x.Name).Must(ValidatorShared.IsName).WithMessage("Name должно содержать только буквы кириллицы, пробел, дефис или точку.");
        RuleFor(x => x.Foodstuffs).NotEmpty();
    }

    // private bool IsGuid(string guid)
    // {
    //     const string pattern = @"[0-9A-Fa-f]{8}-([0-9A-Fa-f]{4}-){3}[0-9A-Fa-f]{12}";
    //     return Regex.IsMatch(guid, pattern, RegexOptions.IgnoreCase);
    // }
}