﻿using CafeMenu.Core.Domain;
using CafeMenu.WebHost.Models;

namespace CafeMenu.WebHost.Mappers;

public static class MapperDish
{
    public static Dish DishFromRequest(DishRequestCreateUpdate request)
    {
        var dst = new Dish(request.Name) { Id = Guid.NewGuid() };
        return CopyDishFromRequest(dst, request);
    }
    
    public static Dish CopyDishFromRequest(Dish dst, DishRequestCreateUpdate request)
    {
        dst.Name = request.Name;
        dst.Description = request.Description;
        dst.Recipe = request.Recipe;
        dst.IsAvailable = request.IsAvailable;
        dst.FoodstuffList = request.Foodstuffs.Select(f => 
            new FoodstuffDish()
            {
                DishId = dst.Id, 
                FoodstuffId = f
            }).ToList();
        return dst;
    }
}