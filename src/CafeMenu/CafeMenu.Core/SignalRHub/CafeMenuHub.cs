﻿using CafeMenu.Core.Domain;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;

namespace CafeMenu.Core.SignalRHub;

public class CafeMenuHub : Hub<IDishesHubClient>
{
    private readonly ILogger<CafeMenuHub> _loggerDishHub;

    public CafeMenuHub(ILogger<CafeMenuHub> loggerDishHub)
    {
        _loggerDishHub = loggerDishHub;
    }

    public async Task NewMessage(List<Dish> dishes)
    {
        //await Clients.All.SendAsync("ReceiveDishes", dishes);
        await Clients.All.ReceiveDishes(dishes);
        _loggerDishHub.LogInformation($"Send to hub {Context.ConnectionId}");
    }
}

public interface IDishesHubClient
{
    Task ReceiveDishes(List<Dish> dishes);
}