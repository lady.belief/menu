﻿namespace CafeMenu.Core.Domain;

public class DishDto
{
    public string Name { get; set; }
    public string? Description { get; set; } // Описание для сайта
    public string? Recipe { get; set; } //Описание процесса приготовления
    public bool IsAvailable  { get; set; } //Доступно ли
    
    public DishDto(Dish dish)
    {
        Name = dish.Name;
        Description = dish.Description;
        Recipe = dish.Recipe;
        IsAvailable = dish.IsAvailable;
    }
}