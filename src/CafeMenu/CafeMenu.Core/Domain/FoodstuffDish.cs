﻿namespace CafeMenu.Core.Domain;

public class FoodstuffDish : BaseEntity
{
    public Guid FoodstuffId { get; set; }
    
    //public double Amount { get; set; } //продукт присутствует в единице блюда в количестве Amount
    public Guid DishId { get; set; }
}