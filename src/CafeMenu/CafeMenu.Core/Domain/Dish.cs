﻿namespace CafeMenu.Core.Domain;

public class Dish : BaseEntity
{
    public string Name { get; set; }
    public string? Description { get; set; } //Описание для сайта
    public string? Recipe { get; set; } //Описание процесса приготовления
    public bool IsAvailable  { get; set; } //Доступно ли
 
    public virtual ICollection<FoodstuffDish> FoodstuffList { get; set; } //Список продуктов
    
    public Dish(string name)
    {
        Name = name;
    }
}