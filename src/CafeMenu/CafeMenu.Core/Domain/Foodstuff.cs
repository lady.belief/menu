﻿namespace CafeMenu.Core.Domain;

public class Foodstuff : BaseEntity
{
    public string Name { get; set; }
    public bool IsAvailable { get; set; }

    public Foodstuff()
    {
        Name = string.Empty;
        IsAvailable = false;
    }
}