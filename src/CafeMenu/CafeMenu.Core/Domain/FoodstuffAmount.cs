﻿namespace Menu.Core.Domain;

public class FoodstuffAmount
{
    public Guid FoodstuffId { get; set; }
    public double Amount { get; set; }
}