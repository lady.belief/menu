﻿using CafeMenu.Core.Services;
using MassTransit;
using Menu.Core.Domain;
using Microsoft.Extensions.Logging;

namespace CafeMenu.Core.Consumers;

public class DishConsumer : IConsumer<FoodstuffDto>
{
    private readonly IFoodstuffService _foodstuffService;
    private readonly ILogger<DishConsumer> _loggerDish;

    public DishConsumer(IFoodstuffService foodstuffService, ILogger<DishConsumer> loggerDish)
    {
        _foodstuffService = foodstuffService;
        _loggerDish = loggerDish;
    }

    public async Task Consume(ConsumeContext<FoodstuffDto> context)
    {
        _loggerDish.LogInformation($"Input message from RabbitMQ FoodstuffId = {context.Message.Id}, IsAvailable = {context.Message.IsAvailable}");
        await _foodstuffService.SetFoodstuffAndDishAvailabilityAsync(context.Message);
    }
}