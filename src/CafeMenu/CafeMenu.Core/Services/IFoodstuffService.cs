﻿using Menu.Core.Domain;

namespace CafeMenu.Core.Services;

public interface IFoodstuffService
{
    public Task SetFoodstuffAndDishAvailabilityAsync(FoodstuffDto request);
    public Task SetDishAvailability(Guid foodstuffId, bool isAvailable);
}