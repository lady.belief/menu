﻿namespace CafeMenu.Core.Services;

public interface ICacheService<T>
{
    public Task<List<T>?> GetFromCache();
    public Task SetToCache(List<T> dishes);
    public Task RemoveCache();
}
