﻿using CafeMenu.Core.Domain;

namespace CafeMenu.Core.Services;

public interface IDishService
{
    public Task UpdateSlices(Guid id, double amount);
    public Task<List<FoodstuffDish>> GetIngredientsAsync(Guid id);
}