﻿using CafeMenu.Core.Abstractions;
using CafeMenu.Core.Domain;
using CafeMenu.Core.SignalRHub;
using Menu.Core.Domain;
using Microsoft.AspNetCore.SignalR;

namespace CafeMenu.Core.Services;
public class FoodstuffDtoAlias : FoodstuffDto
{
    public FoodstuffDtoAlias(Guid id, bool isAvailable) : base(id, isAvailable)
    { }
}

public class FoodstuffService : IFoodstuffService
{
    private readonly IRepository<Foodstuff> _foodstuffRepository;
    private readonly IRepository<Dish> _dishRepository;
    private readonly IRepository<FoodstuffDish> _foodstuffDishRepository;
    private readonly ICacheService<Dish>? _dishCacheService;
    private readonly IHubContext<CafeMenuHub, IDishesHubClient> _hubContextSignalR;

    public FoodstuffService(IRepository<Foodstuff> foodstuffRepository, IRepository<Dish> dishRepository,
        IRepository<FoodstuffDish> foodstuffDishRepository, ICacheService<Dish> dishCacheService, IHubContext<CafeMenuHub, IDishesHubClient> hubContextSignalR)
    {
        _foodstuffRepository = foodstuffRepository;
        _dishRepository = dishRepository;
        _foodstuffDishRepository = foodstuffDishRepository;
        _dishCacheService = dishCacheService;
        _hubContextSignalR = hubContextSignalR;
    }

    public async Task SetFoodstuffAndDishAvailabilityAsync(FoodstuffDto request)
    {
        var foodstuff = await _foodstuffRepository.GetByIdAsync(request.Id);
        if (foodstuff == null) return;

        if (foodstuff.IsAvailable == request.IsAvailable)
            return;
        //Изменить доступность продукта
        foodstuff.IsAvailable = request.IsAvailable;
        await _foodstuffRepository.UpdateAsync(foodstuff);

        await SetDishAvailability(foodstuff.Id, foodstuff.IsAvailable);
    }

    public async Task SetDishAvailability(Guid foodstuffId, bool isAvailable)
    {
        var foodstuffDishAll = (await _foodstuffDishRepository.GetAllAsync()).ToList();
        //Получить все идентификаторы блюд с заданным продуктом
        var dishIds = foodstuffDishAll.Where(fd => fd.FoodstuffId == foodstuffId)
            .Select(fd => fd.DishId).ToList();
        if (!dishIds.Any())
            return;
        
        //Получить список блюд по идентифиаторам
        //var dishes = await _dishRepository.GetRangeByIdsAsync(dishesId); не знаю, как описать последовательность в mock
        var dishes = new List<Dish>();
        foreach (var id in dishIds)
        {
            var d = await _dishRepository.GetByIdAsync(id);
            if(d != default) 
                dishes.Add(d);
        }
        if(!dishes.Any()) return;

        //Изменить доступность блюда
        var foodstuffsAll = (await _foodstuffRepository.GetAllAsync()).ToList();
        foreach (var dish in dishes)
        {
            //Идентификаторы продуктов для блюда
            var foodstuffIds = foodstuffDishAll.Where(fd => fd.DishId == dish.Id)
                .Select(fd => fd.FoodstuffId).ToList();
            //Если все продукты доступны, то блюдо доступно
            var isDishAvailable = foodstuffsAll.Where(f => foodstuffIds.Contains(f.Id)).All(f => f.IsAvailable);

            //Доступность сохранённая и посчитанная совпадают?
            if (dish.IsAvailable != isDishAvailable)
            {
                dish.IsAvailable = isAvailable;
                await _dishRepository.UpdateAsync(dish);
            }
        }

        //Обновить кэш
        if (_dishCacheService != null)
        {
            dishes =  (await _dishRepository.GetAllAsync()).ToList();
            await _dishCacheService.SetToCache(dishes);
        }
        
        //обновить у клиента через SignalR
        if (_hubContextSignalR != null)
        {
            await _hubContextSignalR.Clients.All.ReceiveDishes((await _dishRepository.GetAllAsync()).ToList());
        }
    }
}