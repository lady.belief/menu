﻿using System.Text.Json;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Logging;

namespace CafeMenu.Core.Services;

public class CacheService<T> : ICacheService<T>
{
    private string _cacheKey;
    private readonly IDistributedCache _distributedCache;
    private readonly ILogger<CacheService<T>> _logger;

    public CacheService(IDistributedCache distributedCache, ILogger<CacheService<T>> logger)
    {
        _cacheKey = string.Empty;
        _distributedCache = distributedCache;
        _logger = logger;
    }

    public async Task<List<T>?> GetFromCache()
    {
        var serialized = await _distributedCache.GetStringAsync(_cacheKey);
        if (serialized != null)
        {
            var dateString = DateTime.Now.ToString("dd.MM.yy hh:mm:ss.fff");
            _logger.LogInformation($"{dateString} GET dish list from cache");
            return JsonSerializer.Deserialize<List<T>>(serialized);
        }

        return default;
    }

    public async Task SetToCache(List<T> entities)
    {
        var dateNow = DateTime.Now;
        var absoluteExpiration = dateNow + TimeSpan.FromMinutes(10);
        _cacheKey = typeof(T).Name;
        
        await _distributedCache.SetStringAsync(
            key: _cacheKey,
            value: JsonSerializer.Serialize(entities),
            options: new DistributedCacheEntryOptions
            {
                AbsoluteExpiration = absoluteExpiration
            });
        
        var dateString = dateNow.ToString("dd.MM.yy hh:mm:ss.fff");
        _logger.LogInformation($"{dateString} SET dish list to cache, AbsoluteExpiration = {absoluteExpiration}");
    }

    public async Task RemoveCache()
    {
        await _distributedCache.RemoveAsync(_cacheKey);
    }
}
