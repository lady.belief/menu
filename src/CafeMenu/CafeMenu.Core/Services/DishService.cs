﻿using CafeMenu.Core.Abstractions;
using CafeMenu.Core.Domain;
using MassTransit;
using Menu.Core.Domain;
using Microsoft.Extensions.Logging;

namespace CafeMenu.Core.Services;

public class DishService : IDishService
{
    private readonly IBusControl _busControl;
    private readonly ILogger<DishService> _loggerDish;
    private readonly IRepository<FoodstuffDish> _foodstuffDishRepository;

    public DishService(IBusControl busControl, ILogger<DishService> loggerDish, 
        IRepository<FoodstuffDish> foodstuffDishRepository)
    {
        _busControl = busControl;
        _loggerDish = loggerDish;
        _foodstuffDishRepository = foodstuffDishRepository;
    }

    public async Task UpdateSlices(Guid id, double amount)
    {
        _loggerDish.LogInformation($"Publish message for RabbitMQ FoodstuffId = {id}, Amount = {amount}");
        await _busControl.Publish(new FoodstuffAmount() { FoodstuffId = id, Amount = amount});
    }

    public async Task<List<FoodstuffDish>> GetIngredientsAsync(Guid id)
    {
        var foodstuffDishAll = await _foodstuffDishRepository.GetAllAsync();
        var foodstuffDishList = foodstuffDishAll.Where(d => d.DishId == id).ToList();

        return !foodstuffDishList.Any() ? new List<FoodstuffDish>() : foodstuffDishList;
    }
}