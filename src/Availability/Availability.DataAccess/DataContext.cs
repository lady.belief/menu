﻿using Availability.Core.Domain;
using Microsoft.EntityFrameworkCore;

namespace Availability.DataAccess;

public class DataContext : DbContext
{
    public DbSet<Foodstuff> Foodstuffs { get; set; }
    public DbSet<Provider> Providers { get; set; }
    public DbSet<Reserve> Reserves { get; set; }
    public DbSet<Slice> Slices { get; set; }
    
    public DataContext()
    { }
        
    public DataContext(DbContextOptions<DataContext> options)
        : base(options)
    { }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    { }
}