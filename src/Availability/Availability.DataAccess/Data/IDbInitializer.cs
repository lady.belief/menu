﻿namespace Availability.DataAccess.Data
{
    public interface IDbInitializer
    {
        void InitializeDb();
    }
}