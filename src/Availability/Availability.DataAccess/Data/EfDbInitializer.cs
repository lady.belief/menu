﻿namespace Availability.DataAccess.Data
{
    public class EfDbInitializer
        : IDbInitializer
    {
        private readonly DataContext _dataContext;

        public EfDbInitializer(DataContext dataContext)
        {
            _dataContext = dataContext;
        }
        
        public void InitializeDb()
        {
            _dataContext.Database.EnsureDeleted();
            _dataContext.Database.EnsureCreated();

            _dataContext.AddRange(FakeDataFactory.Foodstuffs);
            _dataContext.SaveChanges();
            
            _dataContext.AddRange(FakeDataFactory.Providers);
            _dataContext.SaveChanges();
            
            _dataContext.AddRange(FakeDataFactory.Slices);
            _dataContext.SaveChanges();
            
            _dataContext.AddRange(FakeDataFactory.Reserves);
            _dataContext.SaveChanges();
        }
    }
}