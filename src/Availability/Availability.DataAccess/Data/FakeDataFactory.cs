﻿using Availability.Core.Domain;

namespace Availability.DataAccess.Data
{
    public static class FakeDataFactory
    {
        public static List<Foodstuff> Foodstuffs => new List<Foodstuff>()
        {
            new Foodstuff()
            {
                Id = Guid.Parse("b52d4a0f-3ecb-4305-9e23-7a7b32773ac4"),
                Name = "ветчина",
                IsAvailable = true
            },
            new Foodstuff()
            {
                Id = Guid.Parse("2669d6dd-5efa-4f1a-b8ce-ad686c2a5b0f"),
                Name = "моцарелла",
                IsAvailable = true
            },
            new Foodstuff()
            {
                Id = Guid.Parse("6421a38f-2679-478d-ad5a-d2c60d31a7a7"),
                Name = "фирменный соус",
                IsAvailable = true
            },
            new Foodstuff()
            {
                Id = Guid.Parse("332ddf3f-ba9f-4407-9a28-37d58f69d921"),
                Name = "цыпленок",
                IsAvailable = true
            },
            new Foodstuff()
            {
                Id = Guid.Parse("89324170-16d4-444e-8822-107d084fd12d"),
                Name = "бекон",
                IsAvailable = false
            },
            new Foodstuff()
            {
                Id = Guid.Parse("453cbff0-88c3-47d9-af80-a18eb7d0c74a"),
                Name = "сыр чеддер",
                IsAvailable = true
            },
            new Foodstuff()
            {
                Id = Guid.Parse("46a84513-6654-4ec8-bf78-41f8642d194b"),
                Name = "сыр пармезан",
                IsAvailable = true
            },
            new Foodstuff()
            {
                Id = Guid.Parse("1d322cc0-8d8a-41aa-b938-90b8601605d7"),
                Name = "томаты",
                IsAvailable = true
            },
            new Foodstuff()
            {
                Id = Guid.Parse("e663c5e4-55b2-4e5c-b977-ce0cd9c89373"),
                Name = "красный лук",
                IsAvailable = true
            },
            new Foodstuff()
            {
                Id = Guid.Parse("91744af7-3e69-4f23-bd76-7b1153e607b7"),
                Name = "чеснок",
                IsAvailable = true
            },
            new Foodstuff()
            {
                Id = Guid.Parse("31915fff-b707-4f82-9b2e-94522fe86e11"),
                Name = "итальянские травы",
                IsAvailable = true
            }
        };

        public static List<Provider> Providers => new List<Provider>()
        {
            new Provider()
            {
                Id = Guid.Parse("fc24734f-2461-41fe-896b-711896a2c504"),
                Name = "ООО Картошка",
                Address = "МосКва",
                Email = "potato@mail.ru",
                Phone = "8-910-111-11-11",
                FoodstuffProvider = new List<FoodstuffProvider>
                {
                    new()
                    {
                        Id = Guid.Parse("f3e71c07-17b2-4c1b-8cf2-573ec7453c19"),
                        FoodstuffId = Guid.Parse("b52d4a0f-3ecb-4305-9e23-7a7b32773ac4"),
                        ProviderId = Guid.Parse("fc24734f-2461-41fe-896b-711896a2c504")
                    }
                }
            }
        };

        public static List<Slice> Slices => new List<Slice>()
        {
            new Slice()
            {
                Id = Guid.Parse("7513e882-5338-4dc5-af6f-e890ba933613"),
                Foodstuff_Id = Guid.Parse("b52d4a0f-3ecb-4305-9e23-7a7b32773ac4"),
                Remainder = 5,
                CriticalValue = 1
            }
        };

        public static List<Reserve> Reserves => new List<Reserve>()
        {
            new Reserve()
            {
                Id = Guid.Parse("ecb429f3-caf1-4256-a3ff-7d38eb78067d"),
                Foodstuff_Id = Guid.Parse("b52d4a0f-3ecb-4305-9e23-7a7b32773ac4"),
                NameFromProvider = "Ветчина Коломенская",
                Remainder = 25,
                CriticalValue = 10
            },
            new Reserve()
            {
                Id = Guid.Parse("a386c3d2-1446-43a6-9c5e-3426484fd66d"),
                Foodstuff_Id = Guid.Parse("89324170-16d4-444e-8822-107d084fd12d"),
                NameFromProvider =  "Бекон по ГОСТ", 
                Remainder = 10,
                CriticalValue = 2
            }
        };
    }
}