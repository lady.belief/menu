using Availability.Core.Domain;
using Availability.DataAccess.Data;
using Availability.WebHost.Controllers;
using Availability.WebHost.Models;
using Microsoft.AspNetCore.Mvc;

namespace AvailabilityTests;

public class ReserveControllerTests
{
    private readonly ReserveController _reserveController;
    private readonly FoodstuffController _foodstuffController;

    public ReserveControllerTests()
    {
        var reserveRepositoryMock = new MockRepositoryBuilder<Reserve>()
            .EnableGetByIdAsync()
            .EnableUpdateAsync()
            .SetData(FakeDataFactory.Reserves)
            .Build();
        var foodstuffRepositoryMock = new MockRepositoryBuilder<Foodstuff>()
            .EnableGetByIdAsync()
            .EnableUpdateAsync()
            .SetData(FakeDataFactory.Foodstuffs)
            .Build();

        _reserveController = new ReserveController(reserveRepositoryMock.Object, foodstuffRepositoryMock.Object);
        _foodstuffController = new FoodstuffController(foodstuffRepositoryMock.Object);
    }
    
    [Fact]
    public void ChangingReserveChangesFoodstuffAvailability()
    {
        var reserveId = Guid.Parse("ecb429f3-caf1-4256-a3ff-7d38eb78067d");

        var reserveRaw = _reserveController.GetReserveAsync(reserveId);
        if ((reserveRaw.Result.Result as OkObjectResult)?.Value is not Reserve reserve)
            return;
        
        var foodstuffRaw = _foodstuffController.GetFoodstuffAsync(reserve.Foodstuff_Id);
        var foodstuff = (foodstuffRaw.Result.Result as OkObjectResult)?.Value as Foodstuff;
        Assert.NotNull(foodstuff);
        Assert.True(foodstuff?.IsAvailable);
            
        var request = Map(reserve);
        request.Remainder = 0; //Запас закончился
        _reserveController.EditReserveAsync(reserveId, request);
        
        foodstuffRaw = _foodstuffController.GetFoodstuffAsync(reserve.Foodstuff_Id);
        foodstuff = (foodstuffRaw.Result.Result as OkObjectResult)?.Value as Foodstuff;
        Assert.NotNull(foodstuff);
        Assert.False(foodstuff?.IsAvailable);
    }

    [Fact]
    public void DeletingReserveChangesFoodstuffAvailability()
    {
        var reserveId = Guid.Parse("ecb429f3-caf1-4256-a3ff-7d38eb78067d");

        var reserveRaw = _reserveController.GetReserveAsync(reserveId);
        if ((reserveRaw.Result.Result as OkObjectResult)?.Value is not Reserve reserve)
            return;
        
        var foodstuffRaw = _foodstuffController.GetFoodstuffAsync(reserve.Foodstuff_Id);
        var foodstuff = (foodstuffRaw.Result.Result as OkObjectResult)?.Value as Foodstuff;
        Assert.NotNull(foodstuff);
        Assert.True(foodstuff?.IsAvailable);
        
        _reserveController.DeleteReserveAsync(reserveId); //Удалили запас
        
        foodstuffRaw = _foodstuffController.GetFoodstuffAsync(reserve.Foodstuff_Id);
        foodstuff = (foodstuffRaw.Result.Result as OkObjectResult)?.Value as Foodstuff;
        Assert.NotNull(foodstuff);
        Assert.False(foodstuff?.IsAvailable);
    }

    private static ReserveRequestCreateUpdate Map(Reserve src)
    {
        var dst = new ReserveRequestCreateUpdate
        {
            Foodstuff_Id = src.Foodstuff_Id,
            NameFromProvider = src.NameFromProvider,
            Remainder = src.Remainder,
            CriticalValue = src.CriticalValue
        };

        return dst;
    }
}