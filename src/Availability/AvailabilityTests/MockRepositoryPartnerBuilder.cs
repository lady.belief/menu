using Availability.Core.Abstractions;
using Availability.Core.Domain;
using Castle.Core.Internal;
using Moq;

namespace AvailabilityTests
{
    public class MockRepositoryBuilder<T> where T : BaseEntity
    {
        public MockRepositoryBuilder<T> SetData(List<T> tList)
        {
            _dataList = tList;
            return this;
        }

        public MockRepositoryBuilder<T> EnableGetByIdAsync()
        {
            _isEnableGetByIdAsync = true;
            return this;
        }

        public MockRepositoryBuilder<T> EnableUpdateAsync()
        {
            _isEnableUpdateAsync = true;
            return this;
        }

        public Mock<IRepository<T>> Build()
        {
            _repositoryMock = new Mock<IRepository<T>>();

            _repositoryMock.Setup(m => m.GetAllAsync()).ReturnsAsync(_dataList);
            foreach (var data in _dataList)
            {
                _repositoryMock.Setup(m => m.DeleteAsync(data)).Callback(() => _dataList.Remove(data));
            }
            
            if (_isEnableGetByIdAsync && !_dataList.IsNullOrEmpty())
            {
                foreach (var data in _dataList)
                {
                    _repositoryMock.Setup(m => m.GetByIdAsync(data.Id)).ReturnsAsync(data);
                }
            }

            //_repositoryMock.Setup(m => m.GetRangeByIdsAsync(It.IsIn(_dataList.Select(d => d.Id).ToList()))).ReturnsAsync(_dataList.Where(d => d.Id == i));

            if (_isEnableUpdateAsync)
            {
                _repositoryMock.Setup(m => m.UpdateAsync(It.IsAny<T>()));
            }

            return _repositoryMock;
        }
        
        private Mock<IRepository<T>> _repositoryMock;
        private List<T> _dataList;
        private bool _isEnableGetByIdAsync;
        private bool _isEnableUpdateAsync;
    }
}