﻿using Availability.Core.Domain;
using Availability.DataAccess.Data;
using Availability.WebHost.Controllers;
using Availability.WebHost.Models;
using Microsoft.AspNetCore.Mvc;

namespace AvailabilityTests;

public class SliceControllerTests
{
    private readonly SliceController _sliceController;
    private readonly ReserveController _reserveController;
    private readonly FoodstuffController _foodstuffController;

    public SliceControllerTests()
    {
        var sliceRepositoryMock = new MockRepositoryBuilder<Slice>()
            .EnableGetByIdAsync()
            .EnableUpdateAsync()
            .SetData(FakeDataFactory.Slices)
            .Build();
        var reserveRepositoryMock = new MockRepositoryBuilder<Reserve>()
            .EnableGetByIdAsync()
            .EnableUpdateAsync()
            .SetData(FakeDataFactory.Reserves)
            .Build();
        var foodstuffRepositoryMock = new MockRepositoryBuilder<Foodstuff>()
            .EnableGetByIdAsync()
            .EnableUpdateAsync()
            .SetData(FakeDataFactory.Foodstuffs)
            .Build();

        _sliceController = new SliceController(sliceRepositoryMock.Object, null, 
            foodstuffRepositoryMock.Object, reserveRepositoryMock.Object);
        _reserveController = new ReserveController(reserveRepositoryMock.Object, foodstuffRepositoryMock.Object);
        _foodstuffController = new FoodstuffController(foodstuffRepositoryMock.Object);
    }

    [Fact]
    public async void CreateSliceWhenExist()
    {
        var foodstuffId = Guid.Parse("b52d4a0f-3ecb-4305-9e23-7a7b32773ac4");

        var sliceAll = await _sliceController.GetSliceAsync();
        var slice = ((sliceAll.Result as OkObjectResult)?.Value as List<Slice>)?
            .First(s => s.Foodstuff_Id == foodstuffId);
        var sliceRemainder = slice.Remainder;

        var reserveAll = await _reserveController.GetReserveAsync();
        var reserve = ((reserveAll.Result as OkObjectResult)?.Value as List<Reserve>)?
            .First(r => r.Foodstuff_Id == foodstuffId);
        var reserveRemainder = reserve.Remainder;
        
        var request = new SliceRequestCreateUpdate()
        {
            Foodstuff_Id = Guid.Parse("b52d4a0f-3ecb-4305-9e23-7a7b32773ac4"),
            Remainder = 4,
            CriticalValue = 3
        };
        
        await _sliceController.CreateSliceAsync(request);

        sliceAll = await _sliceController.GetSliceAsync();
        slice = ((sliceAll.Result as OkObjectResult)?.Value as List<Slice>)?
            .First(s => s.Foodstuff_Id == foodstuffId);
        Assert.NotNull(slice);
        Assert.Equal(sliceRemainder + request.Remainder,slice.Remainder);
        
        reserveAll = await _reserveController.GetReserveAsync();
        reserve = ((reserveAll.Result as OkObjectResult)?.Value as List<Reserve>)?
            .First(r => r.Foodstuff_Id == foodstuffId);
        Assert.NotNull(reserve);
        Assert.Equal(reserveRemainder - request.Remainder, reserve.Remainder);
    }
}