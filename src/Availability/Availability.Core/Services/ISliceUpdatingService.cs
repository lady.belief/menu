﻿using Menu.Core.Domain;

namespace Availability.Core.Services;

public interface ISliceUpdatingService
{
    public Task UpdateSlices(FoodstuffAmount fAmount);
}