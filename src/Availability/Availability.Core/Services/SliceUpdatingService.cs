﻿using Availability.Core.Abstractions;
using Availability.Core.Domain;
using Menu.Core.Domain;

namespace Availability.Core.Services;

public class SliceUpdatingService : ISliceUpdatingService
{
    private readonly IRepository<Slice> _sliceRepository;

    public SliceUpdatingService(IRepository<Slice> sliceRepository)
    {
        _sliceRepository = sliceRepository;
    }

    public async Task UpdateSlices(FoodstuffAmount fAmount)
    {
        var sliceAll = await _sliceRepository.GetAllAsync();
        var slice = sliceAll.FirstOrDefault(s => s.Foodstuff_Id == fAmount.FoodstuffId);
        if(slice == default)
            return;

        var remainder = slice.Remainder - fAmount.Amount;
        slice.Remainder = (remainder >= 0 ? remainder : 0);
        //TODO Если remainder < CriticalValue, отправить оповещение. Если remainder == 0, заблокировать блюдо и продукт

        await _sliceRepository.UpdateAsync(slice);
    }
}