﻿using Availability.Core.Domain;

namespace Availability.Core.Services;

public interface ISliceService
{
    public Task GiveFoodstuffAvailabilityAsync(Foodstuff foodstuff);
}