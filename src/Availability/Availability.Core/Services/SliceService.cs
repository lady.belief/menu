﻿using Availability.Core.Abstractions;
using Availability.Core.Domain;
using MassTransit;
using Menu.Core.Domain;
using Microsoft.Extensions.Logging;

namespace Availability.Core.Services;

public class SliceService : ISliceService
{
    private readonly IBusControl _busControl;
    private readonly ILogger<SliceService> _loggerSlice;

    public SliceService(IBusControl busControl, IRepository<Foodstuff> foodstuffRepository, 
        ILogger<SliceService> loggerSlice)
    {
        _busControl = busControl;
        _loggerSlice = loggerSlice;
    }

    public async Task GiveFoodstuffAvailabilityAsync(Foodstuff foodstuff)
    {
        _loggerSlice.LogInformation($"Publish message for RabbitMQ FoodstuffId = {foodstuff.Id}, isAvailable = {foodstuff.IsAvailable}");
        await _busControl.Publish(new FoodstuffDto(foodstuff.Id, foodstuff.IsAvailable));
    }
} 