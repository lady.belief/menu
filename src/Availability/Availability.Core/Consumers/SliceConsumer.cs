﻿using Availability.Core.Services;
using MassTransit;
using Menu.Core.Domain;
using Microsoft.Extensions.Logging;

namespace Availability.Core.Consumers;

public class SliceConsumer : IConsumer<FoodstuffAmount>
{
    private readonly ISliceUpdatingService _sliceService;
    private readonly ILogger<SliceConsumer> _loggerSlice;

    public SliceConsumer(ISliceUpdatingService sliceService, ILogger<SliceConsumer> loggerSlice)
    {
        _sliceService = sliceService;
        _loggerSlice = loggerSlice;
    }

    public async Task Consume(ConsumeContext<FoodstuffAmount> context)
    {
        _loggerSlice.LogInformation($"Input message from RabbitMQ FoodstuffId = {context.Message.FoodstuffId}, Amount = {context.Message.Amount}");
        await _sliceService.UpdateSlices(context.Message);
    }
}