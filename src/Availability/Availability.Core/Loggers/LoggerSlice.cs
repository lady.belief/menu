﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace Availability.Core.Loggers;

public class LoggerSlice<T> : ILogger<T>
{
    private LogLevel LoggingConfig { get; }
    
    public LoggerSlice(IConfiguration configuration)
    {
        LoggingConfig = ParseLogLevel(configuration);
    }
    
    public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, 
        Exception? exception, Func<TState, Exception?, string> formatter)
    {
        if (!IsEnabled(logLevel)) return;
        
        Console.WriteLine($"[{DateTime.Now}] {logLevel.ToString()[..4]} ({typeof(T)}): EVENT = {eventId}, {formatter(state, exception)} " );
    }

    public bool IsEnabled(LogLevel logLevel)
    {
        return LoggingConfig >= logLevel;
    }

    public IDisposable BeginScope<TState>(TState state)
    {
        return null;
    }

    private static LogLevel ParseLogLevel(IConfiguration configuration)
    {
        var logLevelAppSettings = configuration.GetSection("Logging:LogLevel:Default").Value;
        var i = 0;
        while (i <= (int)LogLevel.None)
        {
            if (string.Equals(((LogLevel)i).ToString(), logLevelAppSettings))
            {
                return (LogLevel)i;
            }
            i++;
        }

        return LogLevel.Trace;
    }
}