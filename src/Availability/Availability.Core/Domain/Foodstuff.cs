﻿namespace Availability.Core.Domain;

public class Foodstuff : BaseEntity
{
    public string Name { get; set; }
    public bool IsAvailable { get; set; }
}