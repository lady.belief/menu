﻿namespace Availability.Core.Domain;

public class Provider : BaseEntity
{
    public string Name { get; set; }
    public string Address { get; set; }
    public string Email { get; set; }
    public string Phone { get; set; }
    
    public virtual ICollection<FoodstuffProvider> FoodstuffProvider { get; set; } //таблица продукт - поставщик
}