﻿namespace Availability.Core.Domain;

public class Slice : BaseEntity
{
    public Guid Foodstuff_Id { get; set; }
    public double Remainder { get; set; } //остаток в граммах (для простоты: для любого блюда используется одинаковое количество ингридиента) 
    public double CriticalValue { get; set; } //критическое значение остатка, когда нужно отправлять оповещение
}