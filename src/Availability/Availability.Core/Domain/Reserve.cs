﻿namespace Availability.Core.Domain;

public class Reserve: BaseEntity
{
    public Guid Foodstuff_Id { get; set; } //колбаса
    public string NameFromProvider { get; set; } //сервелат Московский и СтРаНные БукВы
    public double Remainder { get; set; } //остаток в кг
    public double CriticalValue { get; set; } //критическое значение остатка, когда нужно отправлять оповещение
}