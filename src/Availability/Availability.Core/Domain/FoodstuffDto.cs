﻿namespace Menu.Core.Domain;

public class FoodstuffDto
{
    public Guid Id { get; set; }
    public bool IsAvailable  { get; set; } //Доступно ли
    
    public FoodstuffDto(Guid id, bool isAvailable)
    {
        Id = id;
        IsAvailable = isAvailable;
    }
}