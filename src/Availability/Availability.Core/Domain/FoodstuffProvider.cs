﻿namespace Availability.Core.Domain;

public class FoodstuffProvider : BaseEntity
{
    public Guid FoodstuffId { get; set; }
    public Guid ProviderId { get; set; }
}