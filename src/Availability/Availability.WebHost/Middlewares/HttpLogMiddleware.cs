﻿namespace Availability.WebHost.Middlewares;

public class HttpLogMiddleware
{
    private readonly RequestDelegate _next;
    private const string MessageTemplate = "HTTP Request: Method = {RequestMethod}, Path = {RequestPath}, " +
                                           "Query string = {QueryString}, " +
                                           "Responce: StatusCode = {StatusCode}";

    public HttpLogMiddleware(RequestDelegate next)
    {
        _next = next;
    }

    public async Task InvokeAsync(HttpContext httpContext, ILogger<HttpLogMiddleware> logger)
    {
        await _next(httpContext);
        
        logger.Log(LogLevel.Information, MessageTemplate,
            httpContext.Request.Method, httpContext.Request.Path,
            httpContext.Request.QueryString,
            httpContext.Response?.StatusCode
        );
    }
}

public static class LoggingMiddlewareAppExtensions
{
    public static IApplicationBuilder UseHttpRequestLogging(this IApplicationBuilder applicationBuilder)
    {
        return applicationBuilder.UseMiddleware<HttpLogMiddleware>();
    }
}