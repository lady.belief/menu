﻿using Availability.Core.Abstractions;
using Availability.Core.Domain;
using Availability.WebHost.Models;
using Microsoft.AspNetCore.Mvc;

namespace Availability.WebHost.Controllers;

[ApiController]
[Route("api/v1/[controller]")]
public class FoodstuffController : ControllerBase
{
    private readonly IRepository<Foodstuff> _foodstuffRepository;

    public FoodstuffController(IRepository<Foodstuff> foodstuffRepository)
    {
        _foodstuffRepository = foodstuffRepository;
    }
    
    /// <summary>
    /// Получить список продуктов
    /// </summary>
    /// <returns></returns>
    [HttpGet]
    public async Task<ActionResult<List<Foodstuff>>> GetFoodstuffsAsync()
    {
        var foodstuffs =  await _foodstuffRepository.GetAllAsync();

        return Ok(foodstuffs);
    }
    
    /// <summary>
    /// Получить продукт по id
    /// </summary>
    /// <param name="id">Id продукта, например <example>b52d4a0f-3ecb-4305-9e23-7a7b32773ac4</example></param>
    /// <returns></returns>
    [HttpGet("{id:guid}")]
    public async Task<ActionResult<Foodstuff>> GetFoodstuffAsync(Guid id)
    {
        var foodstuff =  await _foodstuffRepository.GetByIdAsync(id);
        if (foodstuff == null) return NotFound();

        return Ok(foodstuff);
    }
        
    /// <summary>
    /// Создать новый продукт
    /// </summary>
    /// <returns></returns>
    [HttpPost]
    public async Task<IActionResult> CreateFoodstuffAsync(FoodstuffRequestCreateUpdate request)
    {
        await _foodstuffRepository.AddAsync(new Foodstuff()
        {
            Name = request.Name, 
            IsAvailable = request.IsAvailable
        });
        return Ok();
    }
        
    /// <summary>
    /// Обновить продукт
    /// </summary>
    /// <param name="id">Id продукта, например <example>b52d4a0f-3ecb-4305-9e23-7a7b32773ac4</example></param>
    /// <param name="request">Данные запроса></param>
    [HttpPut("{id:guid}")]
    public async Task<IActionResult> EditFoodstuffAsync(Guid id, FoodstuffRequestCreateUpdate request)
    {
        var foodstuff = await _foodstuffRepository.GetByIdAsync(id);
        if (foodstuff == null) return NotFound();

        if (foodstuff.Name == request.Name && foodstuff.IsAvailable == request.IsAvailable)
            return NoContent();

        foodstuff.Name = request.Name;
        if (foodstuff.IsAvailable != request.IsAvailable)
        {
            foodstuff.IsAvailable = request.IsAvailable;
            //TODO обновить блюдо, обновить продукт - отправка в брокер
        }

        await _foodstuffRepository.UpdateAsync(foodstuff);
        return Ok();
    }
        
    /// <summary>
    /// Удалить продукт
    /// </summary>
    /// <param name="id">Id продукта, например <example>b52d4a0f-3ecb-4305-9e23-7a7b32773ac4</example></param>
    [HttpDelete("{id:guid}")]
    public async Task<IActionResult> DeleteFoodstuffAsync(Guid id)
    {
        var foodstuff = await _foodstuffRepository.GetByIdAsync(id);
        if (foodstuff == null) return NotFound();

        //TODO обновить блюдо, удалить продукт - отправка в брокер
        await _foodstuffRepository.DeleteAsync(foodstuff);
        return Ok();
    }
}