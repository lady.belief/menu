﻿using Availability.Core.Abstractions;
using Availability.Core.Domain;
using Availability.DataAccess.Repositories;
using Availability.WebHost.Mappers;
using Availability.WebHost.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Namotion.Reflection;

namespace Availability.WebHost.Controllers;

[ApiController]
[Route("api/v1/[controller]")]
public class ProviderController : ControllerBase
{
    private readonly IRepository<Provider> _providerRepository;
    private readonly IRepository<Foodstuff> _foodstuffRepository;
    private readonly IRepository<FoodstuffProvider> _foodstuffProviderRepository;

    public ProviderController(IRepository<Provider> providerRepository,
        IRepository<FoodstuffProvider> foodstuffProviderRepository, IRepository<Foodstuff> foodstuffRepository)
    {
        _providerRepository = providerRepository;
        _foodstuffProviderRepository = foodstuffProviderRepository;
        _foodstuffRepository = foodstuffRepository;
    }
    
    /// <summary>
    /// Получить список поставщиков
    /// </summary>
    /// <returns></returns>
    [HttpGet]
    public async Task<ActionResult<List<Provider>>> GetProvidersAsync()
    {
        var providers =  await _providerRepository.GetAllAsync();

        return Ok(providers);
    }
    
    /// <summary>
    /// Получить поставщика по id
    /// </summary>
    /// <param name="id">Id поставщика</param>
    /// <returns></returns>
    [HttpGet("{id:guid}")]
    public async Task<ActionResult<Provider>> GetProviderAsync(Guid id)
    {
        var provider =  await _providerRepository.GetByIdAsync(id);
        if (provider == null) return NotFound();

        provider.FoodstuffProvider = new List<FoodstuffProvider>();
        var allFoodstuffProvider = _foodstuffProviderRepository.GetAllAsync();
        foreach (var fp in allFoodstuffProvider.Result.Where(fp => fp.Id == id))
        {
            provider.FoodstuffProvider.Add(fp);
        }
        
        return Ok(provider);
        
    }
        
    /// <summary>
    /// Создать нового поставщика
    /// </summary>
    /// <returns></returns>
    [HttpPost]
    public async Task<IActionResult> CreateProviderAsync(ProviderRequestCreateUpdate request)
    {
        var foodstuffs = await _foodstuffRepository.GetRangeByIdsAsync(request.Foodstuffs);
        var provider = MapperProvider.RequestToProvider(request, foodstuffs);
        await _providerRepository.AddAsync(provider);
        
        return Ok();
    }
        
    /// <summary>
    /// Обновить поставщика
    /// </summary>
    /// <param name="id">Id поставщика</param>
    /// <param name="request">Данные запроса></param>
    [HttpPut("{id:guid}")]
    public async Task<IActionResult> EditProviderAsync(Guid id, ProviderRequestCreateUpdate request)
    {
        var provider = await _providerRepository.GetByIdAsync(id);
        if (provider == null) return NotFound(id);
        
        provider = MapperProvider.CopyRequestToProvider(provider, request, null);
        await _providerRepository.UpdateAsync(provider);
        
        var fpAll = await _foodstuffProviderRepository.GetAllAsync();
        var fpList = fpAll.Where(x => x.ProviderId == id);
        foreach (var fp in fpList)
        {
            await _foodstuffProviderRepository.DeleteAsync(fp);
        }

        var foodstuffs = await _foodstuffRepository.GetRangeByIdsAsync(request.Foodstuffs);
        var fList = foodstuffs.Select(f => new FoodstuffProvider()
        {
            FoodstuffId = f.Id,
            ProviderId = id
        }).ToArray();
        foreach (var f in fList)
        {
            await _foodstuffProviderRepository.AddAsync(f);
        }
        
        return Ok(id);
    }
        
    /// <summary>
    /// Удалить поставщика
    /// </summary>
    /// <param name="id">Id поставщика</param>
    [HttpDelete("{id:guid}")]
    public async Task<IActionResult> DeleteProviderAsync(Guid id)
    {
        var provider = await _providerRepository.GetByIdAsync(id);
        if (provider == null) return NotFound(id);
        
        await _providerRepository.DeleteAsync(provider);
        return Ok(id);
    }
}