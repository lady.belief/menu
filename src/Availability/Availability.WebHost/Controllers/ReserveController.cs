﻿using Availability.Core.Abstractions;
using Availability.Core.Domain;
using Availability.WebHost.Mappers;
using Availability.WebHost.Models;
using Microsoft.AspNetCore.Mvc;

namespace Availability.WebHost.Controllers;

[ApiController]
[Route("api/v1/[controller]")]
public class ReserveController : ControllerBase
{
    private readonly IRepository<Reserve> _reserveRepository;
    private readonly IRepository<Foodstuff> _foodstuffRepository;

    public ReserveController(IRepository<Reserve> reserveRepository, IRepository<Foodstuff> foodstuffRepository)
    {
        _reserveRepository = reserveRepository;
        _foodstuffRepository = foodstuffRepository;
    }
    
    /// <summary>
    /// Получить список доступных остатков
    /// </summary>
    /// <returns></returns>
    [HttpGet]
    public async Task<ActionResult<List<Reserve>>> GetReserveAsync()
    {
        var reserve =  await _reserveRepository.GetAllAsync();

        return Ok(reserve);
    }
    
    /// <summary>
    /// Получить остаток по id
    /// </summary>
    /// <param name="id">Id продукта</param>
    /// <returns></returns>
    [HttpGet("{id:guid}")]
    public async Task<ActionResult<Reserve>> GetReserveAsync(Guid id)
    {
        var reserve =  await _reserveRepository.GetByIdAsync(id);
        if (reserve == null) return NotFound(); 

        return Ok(reserve);
    }
        
    /// <summary>
    /// Создать новый остаток
    /// </summary>
    /// <returns></returns>
    [HttpPost]
    public async Task<IActionResult> CreateReserveAsync(ReserveRequestCreateUpdate request)
    {
        var reserve = new Reserve();
        MapperReserve.CopyToReserve(reserve, request);
        
        await _reserveRepository.AddAsync(MapperReserve.CopyToReserve(reserve, request));
        return Ok();
    }
        
    /// <summary>
    /// Обновить остаток
    /// </summary>
    /// <param name="id">Id остатка</param>
    /// <param name="request">Данные запроса></param>
    [HttpPut("{id:guid}")]
    public async Task<IActionResult> EditReserveAsync(Guid id, ReserveRequestCreateUpdate request)
    {
        var reserve = await _reserveRepository.GetByIdAsync(id);
        if (reserve == null) return NotFound();

        //Обновить доступность продукта
        var foodstuff = await _foodstuffRepository.GetByIdAsync(reserve.Foodstuff_Id);
        var isAvailable = request.Remainder > 0;
        if (foodstuff != null && foodstuff.IsAvailable != isAvailable)
        {
            foodstuff.IsAvailable = isAvailable;
            await _foodstuffRepository.UpdateAsync(foodstuff);            
        }
        
        //Обновить запас
        await _reserveRepository.UpdateAsync(MapperReserve.CopyToReserve(reserve, request));
        return Ok(id);
    }
        
    /// <summary>
    /// Удалить остаток
    /// </summary>
    /// <param name="id">Id остатка</param>
    [HttpDelete("{id:guid}")]
    public async Task<IActionResult> DeleteReserveAsync(Guid id)
    {
        var reserve = await _reserveRepository.GetByIdAsync(id);
        if (reserve == null) return NotFound();

        //Сделать продукт недоступным 
        var foodstuff = await _foodstuffRepository.GetByIdAsync(reserve.Foodstuff_Id);
        if (foodstuff != null)
        {
            foodstuff.IsAvailable = false;
            await _foodstuffRepository.UpdateAsync(foodstuff);            
        }
        
        //Удалить запас
        await _reserveRepository.DeleteAsync(reserve);
        return Ok(id);
    }
}