﻿using Availability.Core.Abstractions;
using Availability.Core.Domain;
using Availability.Core.Services;
using Availability.WebHost.Mappers;
using Availability.WebHost.Models;
using Microsoft.AspNetCore.Mvc;

namespace Availability.WebHost.Controllers;

[ApiController]
[Route("api/v1/[controller]")]
public class SliceController : ControllerBase
{
    private readonly IRepository<Slice> _sliceRepository;
    private readonly IRepository<Foodstuff> _foodstuffRepository;
    private readonly IRepository<Reserve> _reserveRepository;
    private readonly ISliceService _slicesService;

    public SliceController(IRepository<Slice> sliceRepository, ISliceService slicesService, 
        IRepository<Foodstuff> foodstuffRepository, IRepository<Reserve> reserveRepository)
    {
        _sliceRepository = sliceRepository;
        _slicesService = slicesService;
        _foodstuffRepository = foodstuffRepository;
        _reserveRepository = reserveRepository;
    }
    
    /// <summary>
    /// Получить список нарезок
    /// </summary>
    /// <returns></returns>
    [HttpGet]
    public async Task<ActionResult<List<Slice>>> GetSliceAsync()
    {
        var slices =  await _sliceRepository.GetAllAsync();

        return Ok(slices);
    }
    
    /// <summary>
    /// Получить нарезку по id
    /// </summary>
    /// <param name="id">Id продукта, например <example>7513e882-5338-4dc5-af6f-e890ba933613</example></param>
    /// <returns></returns>
    [HttpGet("{id:guid}")]
    public async Task<ActionResult<Slice>> GetSliceAsync(Guid id)
    {
        var slices =  await _sliceRepository.GetByIdAsync(id);

        if (slices != null) return Ok(slices);
        return NotFound();
    }
        
    /// <summary>
    /// Создать новую нарезку
    /// </summary>
    /// <returns></returns>
    [HttpPost]
    public async Task<IActionResult> CreateSliceAsync(SliceRequestCreateUpdate request)
    {
        try
        {
            await UpdateReserve(request.Foodstuff_Id, request.Remainder);
        }
        catch
        {
            return BadRequest();
        }
        
        var sliceAll = await _sliceRepository.GetAllAsync();
        var enumerable = sliceAll as Slice[] ?? sliceAll.ToArray();
        var slice = enumerable.FirstOrDefault(s => s.Foodstuff_Id == request.Foodstuff_Id);
        
        if (slice != default)
        {
            //обновить вместо создания
            await UpdateSlice(slice, new SliceRequestCreateUpdate()
            {
                Foodstuff_Id = request.Foodstuff_Id,
                Remainder = request.Remainder + slice.Remainder,
                CriticalValue = request.CriticalValue
            });
        }
        else
        {
            //Создать, если нарезки такого продукта нет
            await _sliceRepository.AddAsync(new Slice()
            {
                Foodstuff_Id = request.Foodstuff_Id,
                Remainder = request.Remainder,
                CriticalValue = request.CriticalValue
            });
        }

        await SendToBroker(request.Foodstuff_Id, request.Remainder > request.CriticalValue);
        return Ok();
    }
        
    /// <summary>
    /// Обновить нарезку
    /// </summary>
    /// <param name="id">Id нарезки, например <example>7513e882-5338-4dc5-af6f-e890ba933613</example></param>
    /// <param name="request">Данные запроса></param>
    [HttpPut("{id:guid}")]
    public async Task<IActionResult> EditSliceAsync(Guid id, SliceRequestCreateUpdate request)
    {
        var slice = await _sliceRepository.GetByIdAsync(id);
        if (slice == null) return NotFound();

        //Если стало больше, то продукт нарезали. Обновить запас.
        //Если стало меньше, то нарезку потратили, то есть блюдо приготовили. То есть это информация от другого МС. Ничего не делать.
        
        //Обновление резерва
        var wasSliced = request.Remainder - slice.Remainder;
        try
        {
            await UpdateReserve(request.Foodstuff_Id, wasSliced);
        }
        catch
        {
            return BadRequest();
        }
        //Обновление нарезки
        await UpdateSlice(slice, request);
        //Отправка в брокер сообщений для обновления доступности продукта
        await SendToBroker(request.Foodstuff_Id, request.Remainder > request.CriticalValue);
        
        return Ok(id);
    }
        
    /// <summary>
    /// Удалить нарезку
    /// </summary>
    /// <param name="id">Id нарезки, например <example>7513e882-5338-4dc5-af6f-e890ba933613</example></param>
    [HttpDelete("{id:guid}")]
    public async Task<IActionResult> DeleteSliceAsync(Guid id)
    {
        var slice = await _sliceRepository.GetByIdAsync(id);
        if (slice == null) return NotFound();

        await SendToBroker(slice.Foodstuff_Id, false);
        await _sliceRepository.DeleteAsync(slice);
        return Ok(id);
    }

    private async Task UpdateReserve(Guid foodstuffId, double wasSliced)
    {
        if (wasSliced <= 0) return;
        
        var reserveAll = await _reserveRepository.GetAllAsync();
        var reserve = reserveAll.First(r => r.Foodstuff_Id == foodstuffId);
            
        if (reserve.Remainder < wasSliced) throw new ArgumentException("Sliced more than available.");
            
        reserve.Remainder -= wasSliced;
        await _reserveRepository.UpdateAsync(reserve);
        
        //Сделать продукт недоступным, если запас кончился
        if (reserve.Remainder != 0)
            return;
        
        var foodstuff = await _foodstuffRepository.GetByIdAsync(reserve.Foodstuff_Id);
        if (foodstuff != null)
        {
            foodstuff.IsAvailable = false;
            await _foodstuffRepository.UpdateAsync(foodstuff);            
        }
    }

    private async Task UpdateSlice(Slice slice, SliceRequestCreateUpdate request)
    {
        slice.Foodstuff_Id = request.Foodstuff_Id;
        slice.Remainder = request.Remainder;
        slice.CriticalValue = request.CriticalValue;
        await _sliceRepository.UpdateAsync(slice);
    }

    private async Task SendToBroker(Guid foodstuffId, bool isIsAvailable)
    {
        if (_slicesService == null) return;
        
        var foodstuff = await _foodstuffRepository.GetByIdAsync(foodstuffId);
        //Отправка в брокер сообщений для обновления доступности продукта
        await _slicesService.GiveFoodstuffAvailabilityAsync(new Foodstuff() {
            Id = foodstuffId,
            Name = foodstuff?.Name != null ? foodstuff.Name : string.Empty,
            IsAvailable = isIsAvailable});
    }
}