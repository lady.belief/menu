﻿namespace Availability.WebHost.Models;

public class ProviderRequestCreateUpdate
{
    public string Name { get; set; }
    public string Address { get; set; }
    public string Email { get; set; }
    public string Phone { get; set; }
    
    public List<Guid> Foodstuffs { get; set; } //Список продуктов, которые может предоставить поставщик
}