﻿namespace Availability.WebHost.Models;

public class FoodstuffRequestCreateUpdate
{
    public string Name { get; set; }
    public bool IsAvailable { get; set; }
}