﻿namespace Availability.WebHost.Models;

public class SliceRequestCreateUpdate
{
    public Guid Foodstuff_Id { get; set; }
    public double Remainder { get; set; } //остаток в граммах 
    public double CriticalValue { get; set; } //критическое значение остатка, когда нужно отправлять оповещение
}