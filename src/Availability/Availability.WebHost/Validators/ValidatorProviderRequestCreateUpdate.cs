﻿using Availability.WebHost.Models;
using FluentValidation;

namespace Availability.WebHost.Validators;

public class ValidatorProviderRequestCreateUpdate : AbstractValidator<ProviderRequestCreateUpdate>
{
    public ValidatorProviderRequestCreateUpdate()
    {
        RuleFor(x => x.Name).NotEmpty();
        RuleFor(x => x.Name).Must(ValidatorShared.IsName).WithMessage("Name должно содержать только буквы кириллицы, пробел, дефис или точку.");
        RuleFor(x => x.Foodstuffs).NotEmpty();
        RuleFor(x => x.Address).NotEmpty();
        RuleFor(x => x.Email).NotEmpty();
        RuleFor(x => x.Phone).NotEmpty();
    }
}