﻿using Availability.WebHost.Models;
using FluentValidation;

namespace Availability.WebHost.Validators;

public class ValidatorFoodstuffRequestCreateUpdate : AbstractValidator<FoodstuffRequestCreateUpdate>
{
    public ValidatorFoodstuffRequestCreateUpdate()
    {
        RuleFor(x => x.Name).NotEmpty().MaximumLength(150);
        RuleFor(x => x.Name).Must(ValidatorShared.IsName).WithMessage("Name должно содержать только буквы кириллицы, пробел, дефис или точку.");
        RuleFor(x => x.IsAvailable).Equal(false);
    }
}
