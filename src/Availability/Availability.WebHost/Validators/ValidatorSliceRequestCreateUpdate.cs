﻿using Availability.WebHost.Models;
using FluentValidation;

namespace Availability.WebHost.Validators;

public class ValidatorSliceRequestCreateUpdate : AbstractValidator<SliceRequestCreateUpdate>
{
    public ValidatorSliceRequestCreateUpdate()
    {
        RuleFor(x => x.Foodstuff_Id).NotNull().NotEmpty();
        RuleFor(x => x.Remainder).GreaterThanOrEqualTo(0);
        RuleFor(x => x.CriticalValue).GreaterThanOrEqualTo(0);
    }
}