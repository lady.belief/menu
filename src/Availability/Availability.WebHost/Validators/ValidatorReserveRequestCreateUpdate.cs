﻿using Availability.WebHost.Models;
using FluentValidation;

namespace Availability.WebHost.Validators;

public class ValidatorReserveRequestCreateUpdate : AbstractValidator<ReserveRequestCreateUpdate>
{
    public ValidatorReserveRequestCreateUpdate()
    {
        RuleFor(x => x.Foodstuff_Id).NotNull().NotEmpty();
        RuleFor(x => x.NameFromProvider).NotNull().NotEmpty();
        RuleFor(x => x.Remainder).GreaterThanOrEqualTo(0);
        RuleFor(x => x.CriticalValue).GreaterThanOrEqualTo(0);
    }
}