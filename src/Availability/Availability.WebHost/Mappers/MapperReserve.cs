﻿using Availability.Core.Domain;
using Availability.WebHost.Models;

namespace Availability.WebHost.Mappers;

public static class MapperReserve
{
    public static Reserve CopyToReserve(Reserve dst, ReserveRequestCreateUpdate request)
    {
        dst.Foodstuff_Id = request.Foodstuff_Id;
        dst.NameFromProvider = request.NameFromProvider;
        dst.Remainder = request.Remainder;
        dst.CriticalValue = request.CriticalValue;

        return dst;
    }
}