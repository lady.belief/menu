﻿using Availability.Core.Domain;
using Availability.WebHost.Models;

namespace Availability.WebHost.Mappers;

public static class MapperProvider
{
    public static Provider RequestToProvider(ProviderRequestCreateUpdate request, IEnumerable<Foodstuff>? foodstuffs)
    {
        var p = new Provider()
        {
            Id = Guid.NewGuid(),
            FoodstuffProvider = new List<FoodstuffProvider>()
        };
        return CopyRequestToProvider(p, request, foodstuffs);
    }

    public static Provider CopyRequestToProvider(Provider provider, 
        ProviderRequestCreateUpdate request, IEnumerable<Foodstuff>? foodstuffs = default)
    {
        provider.Name = request.Name;
        provider.Address = request.Address;
        provider.Email = request.Email;
        provider.Phone = request.Phone;
        
        if (foodstuffs == default) return provider;
        
        provider.FoodstuffProvider = foodstuffs.Select(f => new FoodstuffProvider()
        {
            Id = Guid.NewGuid(),
            FoodstuffId = f.Id,
            ProviderId = provider.Id
        }).ToList();
        return provider;
    }
}