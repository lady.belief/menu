using System.Reflection;
using Availability.Core.Abstractions;
using Availability.Core.Consumers;
using Availability.Core.Domain;
using Availability.Core.Loggers;
using Availability.Core.Services;
using Availability.DataAccess;
using Availability.DataAccess.Data;
using Availability.DataAccess.Repositories;
using Availability.WebHost.Middlewares;
using Availability.WebHost.Validators;
using FluentValidation.AspNetCore;
using MassTransit;
using Microsoft.EntityFrameworkCore;

namespace Availability.WebHost
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton(typeof(ILogger<SliceService>), typeof(LoggerSlice<SliceService>));
            services.AddSingleton(typeof(ILogger<SliceConsumer>), typeof(LoggerSlice<SliceConsumer>));

            services.AddControllers().AddMvcOptions(x=> 
                x.SuppressAsyncSuffixInActionNames = false);
            
            services.AddScoped(typeof(IRepository<>), typeof(EfRepository<>));
            services.AddScoped<IDbInitializer, EfDbInitializer>();
            
            services.AddSingleton<Foodstuff>();
            
            services.AddDbContext<DataContext>(x =>
            {
                x.UseSqlite("Filename=Availability.sqlite");
            });
            
            services.AddTransient<ISliceService, SliceService>();
            services.AddTransient<ISliceUpdatingService, SliceUpdatingService>();
            services.AddMassTransit(x => {
                x.AddConsumer<SliceConsumer>();
                x.UsingRabbitMq((context, cfg) =>
                {
                    Configure(cfg);
                    RegisterEndPoints(context, cfg);
                });
            });
            services.AddHostedService<MasstransitService>();
            
            services.AddFluentValidation(fv =>
            {
                fv.RegisterValidatorsFromAssemblyContaining<ValidatorFoodstuffRequestCreateUpdate>();
                fv.RegisterValidatorsFromAssemblyContaining<ValidatorProviderRequestCreateUpdate>();
                fv.RegisterValidatorsFromAssemblyContaining<ValidatorReserveRequestCreateUpdate>();
                fv.RegisterValidatorsFromAssemblyContaining<ValidatorSliceRequestCreateUpdate>();
            });
            
            // Register the Swagger services
            services.AddSwaggerDocument(conf => 
                conf.Title = Assembly.GetExecutingAssembly().GetName().Name);
        }
        
        /// <summary>
        /// Конфигурирование
        /// </summary>
        /// <param name="configurator"></param>
        private static void Configure(IRabbitMqBusFactoryConfigurator configurator)
        {
            var hostName = !IsContainer() ? "localhost" : "rabbitmq";
            configurator.Host(hostName, "/",
                h =>
                {
                    h.Username("rabbitForMenu");
                    h.Password("rabbitForMenu");
                    h.Heartbeat(TimeSpan.FromMinutes(1));
                });
        }

        /// <summary>
        /// регистрация эндпоинтов
        /// </summary>
        /// <param name="context"></param>
        /// <param name="configurator"></param>
        private static void RegisterEndPoints(IBusRegistrationContext context, IRabbitMqBusFactoryConfigurator configurator)
        {
            configurator.ReceiveEndpoint($"UpdateSlices", e =>
            {
                e.ConfigureConsumer<SliceConsumer>(context);
                e.UseMessageRetry(r =>
                {
                    r.Incremental(3, TimeSpan.FromSeconds(1), TimeSpan.FromSeconds(1));
                });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IDbInitializer dbInitializer)
        {
            app.UseHttpRequestLogging();
            
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseOpenApi();
            app.UseSwaggerUi3(x =>
            {
                x.DocExpansion = "list";
            });

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
            
            dbInitializer.InitializeDb();
        }

        private static bool IsContainer()
        {
            return bool.TryParse(Environment.GetEnvironmentVariable("DOTNET_RUNNING_IN_CONTAINER"), out var inDocker) && inDocker;
        }
    }
}